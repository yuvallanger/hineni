{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ExtendedDefaultRules #-}

module Main where

import Shelly
import Data.Text as T
default (T.Text)

scanESSIDs :: Sh [Text]
scanESSIDs =
  return . parseIwListScan =<< run "iwlist" ["scan"]
  where
    parseIwListScan :: Text -> [Text]
    parseIwListScan = 
        Prelude.map parseIwListScanESSIDLine
      . Prelude.filter ("ESSID:" `T.isInfixOf`)
      . T.lines

parseIwListScanESSIDLine :: Text -> Text
parseIwListScanESSIDLine x =
  let (_, quotedESSID)  = T.break (=='"') x
      (_, anESSIDquote) = T.splitAt 1 quotedESSID
      (essid, _)        = T.break (=='"') anESSIDquote
  in essid

main :: IO ()
main = do
  let wantedESSIDs = ["FantasticCrate"]
  scannedESSIDs <- shelly . silently $ scanESSIDs
  let hineni =
       case [() | x <- scannedESSIDs
                , y <- wantedESSIDs
                , x == y]
       of [] -> False
          _  -> True
  print hineni
